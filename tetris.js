const canvas = document.getElementById('gameMap');
const ctx = canvas.getContext('2d');
// const keycode = require('keycode');
//NOTICE:
//I call each block of a piece a dot

var comingPiece = [];
var pieceCreated = [10, 10];
var map = [];
var preMatrix = [];
var level = 1;
var lose = false;
var pauseGame = false;

//--- variables about the basic map
const backGroundcolor = '#E0E0E0';
const us = 25;
const row = 23;
const column = 10;
baseX = 15;
baseY = 0;
//2 base values are the start point of the play field

//--- variables about the images:
const scoreBoardColor = '#34568b'; //---SCORE BOARD BG COLOR
const boardBorderColor = 'black'; //--- ALL THE BORDER COLOR
const playFieldBorderColor = '#34568b' //---BORDER OF PLAY FIELD
const borderColor = '#E0E0E0';
const innerColor = 'white';
const circleColor = 'grey';
const effectWidth = us / 4;

const boardWidth = 8 * us;
const boardHeight = 9 * us;
const rangeFromGamefieldToBoard = 2 * us;
const boardBorderWidth = us / 2;
const preSightWidth = us / 4;

var spaceFromTop = 3 * us;

const nextPieceBoardWidth = 8 * us;
const nextPieceBoardHeight = row * us - boardHeight - us - 3 * us;
//---variables about the game flow
var moving = false;
var start = true;
var rotated = [];
var speed = 1000;

//---variables about the next pieces
var next = [];
var nextCreated = false;

//---variables about the score
var score = 0;
var lines = 0;
var tetris = 0;

//---for 2 players:
const spaceBetween2Field = 25 * us;

//---for the keyboard:
document.addEventListener('keydown', (event) => {
    var code = event.which;
    //--- function with the code
    if (code == 65 || code == 37) {
        moveLeft(piece);
    };
    if (code == 68 || code == 39) {
        moveRight(piece);
    };
    if (code == 87 || code == 38) {
        rotate(piece);
    };
    if (code == 83 || code == 40) {
        hardDrop();
    };
}, false);

//---THE COLOR LIST: 
const cyan = `rgb(
    ${0},
    ${255}, 
    ${255}
)`;

const blue = `rgb(
    ${0},
    ${0}, 
    ${225}
)`

const orange = `rgb(
    ${255},
    ${128}, 
    ${0}
)`

const yellow = `rgb(
    ${255},
    ${255}, 
    ${0}
)`

const green = `rgb(
    ${0},
    ${255}, 
    ${0}
)`

const purple = `rgb(
    ${255},
    ${0}, 
    ${255}
)`

const red = `rgb(
    ${255},
    ${0}, 
    ${0}
)`
const colorList = [
    cyan,
    blue,
    orange,
    yellow,
    green,
    purple,
    red,
]

//---THE PIECE LIST:
const I = [
    [0, 0, 1, 0],
    [0, 0, 1, 0],
    [0, 0, 1, 0],
    [0, 0, 1, 0]
];

const J = [
    [0, 0, 0, 0],
    [0, 1, 0, 0],
    [0, 1, 1, 1],
    [0, 0, 0, 0]
];

const L = [
    [0, 0, 0, 0],
    [0, 0, 0, 1],
    [0, 1, 1, 1],
    [0, 0, 0, 0]
];

const O = [
    [0, 0, 0, 0],
    [0, 1, 1, 0],
    [0, 1, 1, 0],
    [0, 0, 0, 0]
];

const S = [
    [0, 0, 0, 0],
    [0, 0, 0, 0],
    [0, 0, 1, 1],
    [0, 1, 1, 0]
];

const T = [
    [0, 0, 0, 0],
    [0, 0, 0, 0],
    [0, 1, 1, 1],
    [0, 0, 1, 0]
];

const Z = [
    [0, 0, 0, 0],
    [0, 0, 0, 0],
    [0, 1, 1, 0],
    [0, 0, 1, 1]
];

const pieceList = [I, J, L, O, S, T, Z]; //---7 pieces
//---All above are basic pieces

//---about the title of the game:
function createTitle() {
    ctx.fillStyle = 'blue';
    ctx.font = 'bold 40px Roboto';
    ctx.fillText('TETRIS GAME !', baseX + 2 * us, baseY + 2 * us, column * row - 2 * (baseX + 0.5 * us));
}

//---about the score board:
function updateScore() { //--- create the scoreBoard every time a piece stops
    ctx.fillStyle = boardBorderColor;
    ctx.fillRect(
        baseX + column * us + rangeFromGamefieldToBoard,
        baseY + spaceFromTop,
        boardWidth,
        boardHeight
    )
    ctx.fillStyle = scoreBoardColor;
    ctx.fillRect(
        baseX + column * us + rangeFromGamefieldToBoard + boardBorderWidth / 2,
        baseY + boardBorderWidth / 2 + spaceFromTop,
        boardWidth - boardBorderWidth,
        boardHeight - boardBorderWidth
    )
    ctx.fillStyle = 'white';
    ctx.font = 'bold 40px Arial';
    ctx.fillText(
        'LEVEL: ' + String(level),
        baseX + column * us + rangeFromGamefieldToBoard + us / 2,
        baseY + boardBorderWidth / 2 + 2 * us - 0.5 * us + spaceFromTop,
        boardWidth - us
    )
    ctx.font = 'bold italic 28px Times New Roman';
    ctx.fillText(
        'SCORE: ' + String(score),
        baseX + column * us + rangeFromGamefieldToBoard + us / 2,
        baseY + boardBorderWidth / 2 + 4 * us - 0.5 * us + spaceFromTop
    )
    ctx.fillText(
        'LINES: ' + String(lines),
        baseX + column * us + rangeFromGamefieldToBoard + us / 2,
        baseY + boardBorderWidth / 2 + 6 * us - 0.5 * us + spaceFromTop
    )
    ctx.fillText(
        'TETRIS: ' + String(tetris),
        baseX + column * us + rangeFromGamefieldToBoard + us / 2,
        baseY + boardBorderWidth / 2 + 8 * us - 0.5 * us + spaceFromTop
    )
}

//---board for the next piece:
function updateNextPieceBoard() { //--- create the scoreBoard every time a piece stops
    let space = us / 4;
    let align = us;
    ctx.fillStyle = boardBorderColor;
    ctx.fillRect(
        baseX + column * us + rangeFromGamefieldToBoard,
        baseY + boardHeight + us + spaceFromTop,
        nextPieceBoardWidth,
        nextPieceBoardHeight
    )
    ctx.fillStyle = scoreBoardColor;
    ctx.fillRect(
        baseX + column * us + rangeFromGamefieldToBoard + boardBorderWidth / 2,
        baseY + boardHeight + us + boardBorderWidth / 2 + spaceFromTop,
        nextPieceBoardWidth - boardBorderWidth,
        nextPieceBoardHeight - boardBorderWidth
    )
    ctx.fillStyle = 'white';
    ctx.font = 'Bold 26px Times New Roman';
    ctx.fillText(
        'NEXT BLOCK: ',
        baseX + column * us + rangeFromGamefieldToBoard + us / 2,
        baseY + boardBorderWidth / 2 + 2.3 * us + boardHeight + spaceFromTop,
        7 * us
    )
    for (var y = 0; y < next.length; y++) {
        for (var x = 0; x < next[y].length; x++) {
            if (next[y][x]['status'] == 1) {
                ctx.fillStyle = backGroundcolor;
                ctx.fillRect(
                    baseX + column * us + rangeFromGamefieldToBoard + us / 2 + x * us - 3 * us + align,
                    baseY + boardBorderWidth / 2 + 2.3 * us + boardHeight + y * us + space + spaceFromTop,
                    us,
                    us
                )
                ctx.fillStyle = next[y][x]['color'];
                ctx.fillRect(
                    baseX + column * us + rangeFromGamefieldToBoard + us / 2 + x * us - 3 * us + align + effectWidth / 4,
                    baseY + boardBorderWidth / 2 + 2.3 * us + boardHeight + y * us + space + effectWidth / 4 + spaceFromTop,
                    us - effectWidth / 2,
                    us - effectWidth / 2
                )
            }
        }
    }
}

//---make the play field looks better:
function createBorderForPlayField() {
    ctx.fillStyle = playFieldBorderColor;
    ctx.fillRect(0, spaceFromTop, baseX, row * us - spaceFromTop);
    ctx.fillRect(column * us + baseX, spaceFromTop, baseX, row * us - spaceFromTop);
    ctx.fillRect(0, row * us, column * us + 2 * baseX, baseX)
}

//Move the piece to the mid of the play field
for (shape in pieceList) {
    let arr = [];
    for (var times = 0; times < 3; times++) {
        for (var y = 0; y < pieceList[shape].length; y++) {
            pieceList[shape][y].unshift({
                status: 0,
                color: backGroundcolor
            })
        }
    }
    for (var time = 0; time < pieceList[shape].length; time++) {
        arr.push(
            {
                status: 0,
                color: backGroundcolor
            }
        )
    }
}

//---FUNCTION SHOW THE DESTINATION OF THE CURRENT PIECE:
function preBlock(matrix) {

    //--- the array: add to preMatrix if it's not the max
    let arr = [];
    for (var i = 0; i < matrix[0].length; i++) {
        arr.push(
            {
                status: 0,
                color: backGroundcolor
            }
        );
    }

    //---create the preMatrix = matrix first
    preMatrix = [];
    let max = false;
    for (var y = 0; y < matrix.length; y++) {
        preMatrix.push([]);
        for (var x = 0; x < matrix[y].length; x++) {
            preMatrix[y].push(
                {
                    status: matrix[y][x]['status'],
                    color: matrix[y][x]['color']
                }
            );
        }
    }
    //--- conditions to get the complete preMatrix
    for (var n = 0; n < row; n++) {
        max = false; //---max is true when the preMatrix find its limit
        for (var y = 0; y < preMatrix.length; y++) {
            for (var x = 0; x < preMatrix[y].length; x++) {
                if (preMatrix[y][x]['status'] == 1) {
                    if (map[y + 1] != undefined) {
                        if (map[y + 1][x]['status'] == 1) {
                            max = true;
                            break;
                        }
                    } else {
                        max = true;
                        break;
                    }
                }
            }
            if (max == true) { break; }
        }
        if (max == false) {
            preMatrix.unshift(arr); //---if it's not true, put it down 1 more line
        }
        else {
            if (max == true) {
                break;
            }
        }
    }
}

//This part add the status and color to pieces
for (piece in pieceList) {
    for (y in pieceList[piece]) {
        for (x in pieceList[piece][y]) {
            let status = pieceList[piece][y][x];
            let color = colorList[piece];
            pieceList[piece][y][x] = {}; //---every dot is a object
            pieceList[piece][y][x]['status'] = status;
            if (pieceList[piece][y][x]['status'] == 1) {
                pieceList[piece][y][x]['color'] = color;
            };
            if (pieceList[piece][y][x]['status'] == 0) {
                pieceList[piece][y][x]['color'] = backGroundcolor;
            };
        }
    }
}

//---So now I have each piece is a matrix; a dot = status + color

//function that create the map matrix
function createMapMatrix() {
    for (var y = 0; y < row; y++) {
        map.push([]);
        for (var x = 0; x < column; x++) {
            map[y].push({});
            map[y][x]['status'] = 0;
            map[y][x]['color'] = backGroundcolor;
        }
    }
}

//function to draw 
function draw(matrix) { //--- this just draw the matrix, no effects
    for (y in matrix) {
        for (x in matrix[y]) {
            ctx.fillStyle = matrix[y][x]['color'];
            ctx.fillRect(baseX + x * us, baseY + y * us, us, us);
        }
    } //--- end of loop through all dots
}

//---draw the preMatrix
function preSight() { //--- Border outside
    for (var y = 0; y < preMatrix.length; y++) {
        for (var x = 0; x < preMatrix[y].length; x++) {
            if (preMatrix[y][x]['status'] == 1) {
                ctx.fillStyle = '#FF9999';
                ctx.fillRect(
                    baseX + x * us,
                    baseY + y * us,
                    us,
                    us
                );
            }
        }
    } //--- end of loop through all dots
}

//function to create piece's effect:  //--- something like create border or a circle inside the piece
function effect1(matrix) { //--- Border outside
    for (y in matrix) {
        if (y > 2) {
            for (x in matrix[y]) {
                if (matrix[y][x]['status'] == 1 ||
                    matrix == map
                ) {
                    ctx.fillStyle = borderColor;
                    ctx.fillRect(
                        baseX + x * us,
                        baseY + y * us,
                        us,
                        us
                    );
                    ctx.fillStyle = matrix[y][x]['color'];
                    ctx.fillRect(
                        baseX + x * us + effectWidth / 2,
                        baseY + y * us + effectWidth / 2,
                        us - effectWidth,
                        us - effectWidth
                    )
                }
                if (matrix[y][x]['status'] == 1) { //---create the shadow, make piece looks more real
                    ctx.fillStyle = 'white'; //---white part
                    ctx.beginPath();
                    ctx.moveTo(
                        baseX + x * us,
                        baseY + y * us
                    );
                    ctx.lineTo(
                        baseX + x * us + us,
                        baseY + y * us
                    )
                    ctx.lineTo(
                        baseX + x * us + us - effectWidth / 2,
                        baseY + y * us + effectWidth / 2
                    )
                    ctx.lineTo(
                        baseX + x * us + effectWidth / 2,
                        baseY + y * us + effectWidth / 2
                    )
                    ctx.lineTo(
                        baseX + x * us,
                        baseY + y * us
                    )
                    ctx.fill();

                    ctx.fillStyle = 'black'; //---black part
                    ctx.beginPath();
                    ctx.moveTo(
                        baseX + x * us,
                        baseY + y * us + us
                    );
                    ctx.lineTo(
                        baseX + x * us + us,
                        baseY + y * us + us
                    )
                    ctx.lineTo(
                        baseX + x * us + us - effectWidth / 2,
                        baseY + y * us + us - effectWidth / 2
                    )
                    ctx.lineTo(
                        baseX + x * us + effectWidth / 2,
                        baseY + y * us - effectWidth / 2 + us
                    )
                    ctx.lineTo(
                        baseX + x * us,
                        baseY + y * us + us
                    )
                    ctx.fill();

                    //---2 sides part
                    ctx.fillStyle = 'black';
                    ctx.moveTo(
                        baseX + x * us,
                        baseY + y * us
                    );
                    ctx.lineTo(
                        baseX + x * us,
                        baseY + y * us + us
                    )
                    ctx.lineTo(
                        baseX + x * us + effectWidth / 2,
                        baseY + y * us + us - effectWidth / 2
                    )
                    ctx.lineTo(
                        baseX + x * us + effectWidth / 2,
                        baseY + y * us + effectWidth / 2
                    )
                    ctx.lineTo(
                        baseX + x * us,
                        baseY + y * us
                    )
                    ctx.fill();
                }
            }
        }
    } //--- end of loop through all dots
}

function effect2(matrix) { //--- Color inside
    for (y in matrix) {
        for (x in matrix[y]) {
            if (
                matrix[y][x]['status'] != undefined &&
                matrix[y][x]['status'] != 0 ||
                matrix == map
            ) {
                ctx.fillStyle = matrix[y][x]['color'];
                ctx.fillRect(
                    baseX + x * us,
                    baseY + y * us,
                    us,
                    us
                );
                ctx.fillStyle = backGroundcolor;
                ctx.fillRect(
                    baseX + x * us + effectWidth / 2,
                    baseY + y * us + effectWidth / 2,
                    us - effectWidth,
                    us - effectWidth
                );
            }
        }
    } //--- end of loop through all dots
}

//---MOVING FUNCTION BELOW:

//---move down:
function moveDown(matrix) {
    let able = true;
    for (var y = 0; y < matrix.length; y++) {
        for (var x = 0; x < matrix[y].length; x++) {
            if (
                matrix[y][x]['status'] == 0 && map[y] == undefined ||
                matrix[y][x]['status'] == 1 && map[1 + y] == undefined ||
                matrix[y][x]['status'] == 1 && map[1 + y][x]['status'] == 1
            ) {
                able = false;
                break;
            }
        }
        if (able == false) {
            moving = false;
            break;
        }
    }
    if (able == true) {
        let arr = [];
        for (var i = 0; i < matrix[0].length; i++) {
            arr.push({});
            arr[i]['status'] = 0;
            arr[i]['color'] = backGroundcolor;
        }
        matrix.unshift(arr);
    }
    preBlock(matrix);
}

//---to the left:
function moveLeft(matrix) {
    if (moving == true && lose == false) {
        let able = true;
        for (y in matrix) {
            for (x in matrix[y]) {
                if (
                    matrix[y][x]['status'] == 1 && map[y][x - 1]['status'] == undefined ||
                    matrix[y][x]['status'] == 1 && map[y][x - 1]['status'] == 1
                ) {
                    able = false;
                    break;
                }
            }
            if (able == false) { break; }
        }
        if (able == true) {
            for (y in matrix) {
                matrix[y].shift()
            }
        }
        effect1(map);
        effect1(matrix);
        createBorderForPlayField();
        preBlock(matrix);
        preSight();
    } //---end of check if moving is true
}

//---to the right:
function moveRight(matrix) {
    if (moving == true && lose == false) {
        let able = true;
        for (var y = 0; y < matrix.length; y++) {
            for (var x = 0; x < matrix[y].length; x++) {
                if (
                    matrix[y][x]['status'] == 1 && map[y][x + 1] == undefined ||
                    matrix[y][x]['status'] == 1 && map[y][x + 1]['status'] == 1
                ) {
                    able = false;
                    break;
                }
            }
            if (able == false) { break; }
        }
        if (able == true) {
            for (var y = 0; y < matrix.length; y++) {
                matrix[y].unshift(
                    {
                        status: 0,
                        color: backGroundcolor
                    }
                );
            }
        }
        effect1(map);
        effect1(matrix);
        createBorderForPlayField();
        preBlock(matrix);
        preSight();
    } //---end of check if moving is true
}

//--- to rotate the piece:
function rotate(matrix) {
    if (lose == false) {
        //---case I / O: 
        if (
            matrix[0].length > 3 &&
            (
                matrix[matrix.length - 4][matrix[0].length - 2]['status'] == 1 ||
                matrix[matrix.length - 2][matrix[0].length - 4]['status'] == 1 ||
                matrix[matrix.length - 2][matrix[0].length - 2]['status'] == 1 &&
                matrix[matrix.length - 2][matrix[0].length - 3]['status'] == 1 &&
                matrix[matrix.length - 3][matrix[0].length - 2]['status'] == 1 &&
                matrix[matrix.length - 3][matrix[0].length - 3]['status'] == 1
            ) ||
            (
                matrix[matrix.length - 2][matrix[0].length - 2]['status'] == 1 &&
                matrix[matrix.length - 2][matrix[0].length - 3]['status'] == 1 &&
                matrix[matrix.length - 3][matrix[0].length - 2]['status'] == 1 &&
                matrix[matrix.length - 3][matrix[0].length - 3]['status'] == 1
            ) ||
            (
                matrix[matrix.length - 2][matrix[0].length - 2]['status'] == 1 &&
                matrix[matrix.length - 2][matrix[0].length - 1]['status'] == 1 &&
                matrix[matrix.length - 3][matrix[0].length - 2]['status'] == 1 &&
                matrix[matrix.length - 3][matrix[0].length - 1]['status'] == 1
            )
        ) { //--- the condition to check if the piece is I or O 
            if (moving == true) {
                let able = true;
                var rotated = [];
                for (var y = 0; y < matrix.length; y++) {
                    rotated.push([]);
                    for (x = 0; x < matrix[y].length; x++) {
                        rotated[y].push({
                            status: 0,
                            color: 'black'
                        });
                    }
                }
                for (var y = 4; y > 0; y--) {
                    for (var x = 4; x > 0; x--) {
                        rotated[rotated.length - y][rotated[0].length - x]['status'] = matrix[matrix.length - x][matrix[0].length - y]['status'];
                        rotated[rotated.length - y][rotated[0].length - x]['color'] = matrix[matrix.length - x][matrix[0].length - y]['color'];
                        if (
                            rotated[rotated.length - y][rotated[0].length - x]['status'] == 1 &&
                            map[rotated.length - y][rotated[0].length - x]['status'] == 1 ||
                            rotated[rotated.length - y][rotated[0].length - x]['status'] == 1 &&
                            map[rotated.length - y][rotated[0].length - x] == undefined
                        ) {
                            able = false;
                        }
                    }
                }
                if (able == true) {
                    for (var y = 0; y < matrix.length; y++) {
                        for (var x = 0; x < matrix[y].length; x++) {
                            matrix[y][x]['status'] = rotated[y][x]['status'];
                            matrix[y][x]['color'] = rotated[y][x]['color'];
                        }
                    }
                    effect1(map);
                    effect1(matrix);
                    preBlock(matrix);
                    preSight();
                    createBorderForPlayField();
                }
            }
        }
        //---end of case I / O 

        //---case I near the border
        else if (
            matrix[0].length <= 3 &&
            (
                (
                    matrix[matrix.length - 1][matrix[0].length - 2]['status'] == 1 &&
                    matrix[matrix.length - 2][matrix[0].length - 2]['status'] == 1 &&
                    matrix[matrix.length - 3][matrix[0].length - 2]['status'] == 1 &&
                    matrix[matrix.length - 4][matrix[0].length - 2]['status'] == 1
                ) ||
                (
                    matrix[matrix.length - 1][matrix[0].length - 1]['status'] == 1 &&
                    matrix[matrix.length - 2][matrix[0].length - 1]['status'] == 1 &&
                    matrix[matrix.length - 3][matrix[0].length - 1]['status'] == 1 &&
                    matrix[matrix.length - 4][matrix[0].length - 1]['status'] == 1
                )
            )
        ) {
            if (matrix[0].length == 3) {
                for (var y = 0; y < matrix.length; y++) {
                    matrix[y].unshift(
                        {
                            status: 0,
                            color: backGroundcolor
                        }
                    )
                }
                rotate(matrix);
            }
            else {
                for (var times = 0; times < 3; times++) {
                    for (var y = 0; y < matrix.length; y++) {
                        matrix[y].unshift(
                            {
                                status: 0,
                                color: backGroundcolor
                            }
                        );
                    }
                }
                rotate(matrix);
            }
        }
        else { // other pieces 
            if (moving == true) {
                let able = true;
                var rotated = [];
                for (var y = 0; y < matrix.length; y++) {
                    rotated.push([]);
                    for (x = 0; x < matrix[y].length; x++) {
                        rotated[y].push({
                            status: 0,
                            color: 'black'
                        });
                    }
                }
                for (var y = 1; y < 4; y++) {
                    for (var x = 1; x < 4; x++) {
                        rotated[rotated.length - y][rotated[0].length - x]['status'] = matrix[matrix.length - (4 - x)][matrix[0].length - y]['status'];
                        rotated[rotated.length - y][rotated[0].length - x]['color'] = matrix[matrix.length - (4 - x)][matrix[0].length - y]['color'];
                        if (
                            rotated[rotated.length - y][rotated[0].length - x]['status'] == 1 &&
                            map[rotated.length - y][rotated[0].length - x]['status'] == 1 //||
                            // rotated[rotated.length-y][rotated[0].length-x]['status'] == 1 &&
                            // map[rotated.length-y][rotated[0].length-x] == undefined 
                        ) {
                            able = false;
                        }
                    }
                }
                if (able == true) {
                    for (var y = 0; y < matrix.length; y++) {
                        for (var x = 0; x < matrix[y].length; x++) {
                            matrix[y][x]['status'] = rotated[y][x]['status'];
                            matrix[y][x]['color'] = rotated[y][x]['color'];
                        }
                    }
                    effect1(map);
                    effect1(matrix);
                    preBlock(matrix);
                    preSight();
                    createBorderForPlayField();
                }
            }
        } //--- end of case other pieces (with out I / O)
    }
}

//---to drop the piece:
function hardDrop() {
    if (lose == false) {
        piece = [];
        for (var y = 0; y < preMatrix.length; y++) {
            piece.push([]);
            for (var x = 0; x < preMatrix[y].length; x++) {
                piece[y].push(
                    {
                        status: preMatrix[y][x]['status'],
                        color: preMatrix[y][x]['color']
                    }
                )
            }
        }
        effect1(map);
        effect1(piece);
        check();
        createBorderForPlayField();
    }
}

//---FUNCTION ABOUT THE GAME LOGIC: 

//---function to check if lose:
function checkIfLose() {
    if (map[4] != undefined) {
        for (var x = 3; x < 7; x++) {
            if (map[4][x]['status'] == 1) {
                lose = true;
            }
        }
    }
}

//---function to check if a row is full (then break it) 
function updateMap(num) {
    for (var y = num; y > 0; y--) {
        for (var x = 0; x < map[num].length; x++) {
            let status = map[y][x]['status'];
            let color = map[y][x]['color'];
            map[y][x]['status'] = map[y - 1][x]['status'];
            map[y - 1][x]['status'] = status;
            map[y][x]['color'] = map[y - 1][x]['color'];
            map[y - 1][x]['color'] = color;
        }
    }
}

function check() {
    if (1 + Math.floor(lines / 10) > level) {
        speedUp();
        level = 1 + Math.floor(lines / 10);
    }

    let streak = 0;
    let currentScore = 40;
    let times = 1.25;
    let totalScore = 0;
    if (moving == false) {
        for (var y = map.length - 1; y > 0; y--) {
            let full = true;
            for (var x = 0; x < map[y].length; x++) {
                if (map[y][x]['status'] == 0) {
                    full = false;
                }
            }
            if (full == true) {
                for (var x = 0; x < map[y].length; x++) {
                    map[y][x]['status'] = 0;
                    map[y][x]['color'] = backGroundcolor;
                }

                streak += 1;
                currentScore = 40 * level;
                totalScore += currentScore;
                if (streak == 1) { times = 1 };
                if (streak == 2) { times = 1.25 };
                if (streak == 3) { times = 2.5 };
                if (streak == 4) {
                    times = 7.5;
                    tetris += 1;
                }

                lines += 1;
                updateMap(y);
                y += 1;
            }
        }
        totalScore = totalScore * times;
        score += totalScore;
    }
}

//---function to cleann the matrix:
function clean(matrix) {
    let ok = true;
    let able = false;
    let cleanable = true;
    for (var y = 1; y < matrix.length; y++) {
        for (var x = 0; x < matrix[y].length; x++) {
            if (matrix[y][x]['status'] == 1) {
                able = true;
            }
            if (able == true) {
                if (matrix[y][x]['status'] == 1) {
                    cleanable = false;
                }
            }
        }
    }
    if (cleanable == true) {
        matrix.pop();
        ok = false;
    }
    if (ok == false) {
        clean(matrix);
    }
}

//---function to create random piece
function createPiece() {
    let arr = [];
    let num = Math.floor(Math.random() * pieceList.length)
    while (num == pieceCreated[pieceCreated.length - 1] && num == pieceCreated[pieceCreated.length - 2]) {
        num = Math.floor(Math.random() * pieceList.length);
    }
    for (var y = 0; y < pieceList[num].length; y++) {
        arr.push([]);
        for (var x = 0; x < pieceList[num][y].length; x++) {
            arr[y].push({
                status: pieceList[num][y][x]['status'],
                color: pieceList[num][y][x]['color']
            })
        }
    }
    pieceCreated.push(num);
    return arr;
}

//---function to save the old piece: combine old piece with the map
function combine(matrix) {
    // clean(matrix);
    for (var y = 0; y < matrix.length; y++) {
        for (var x = 0; x < matrix[y].length; x++) {
            if (matrix[y][x]['status'] != 0 && map[y] != undefined) {
                if (
                    map[y][x]['status'] == 0 &&
                    matrix[y][x]['status'] == 1
                ) {
                    map[y][x]['status'] = 1;
                    map[y][x]['color'] = matrix[y][x]['color'];
                };
            }
        }
    }
}

//---function to restart:
//---no idea

function updateComingPiece() { //---THIS FOR CREATING THE UPCOMING PIECE LIST

    comingPiece.push(createPiece());
}
//---PLAYER WILL GET NEW PIECE FROM THIS
updateComingPiece();
updateComingPiece();
setInterval(updateComingPiece, 10);

moving = false;
createMapMatrix();
var piece = [];
var next = [];
var index = 0;
next = comingPiece[index];
var ready = true;

function gameRun() {
    checkIfLose();
    if (lose == false) {
        if (moving == false) {
            //---merge the current piece to the map
            combine(piece);

            //---check if there's a score, then update the board
            check();
            updateScore();
            updateNextPieceBoard();

            //---draw the map
            effect1(map);

            //---create new piece
            piece = [];

            //---the piece = the next
            for (var y = 0; y < next.length; y++) {
                piece.push([]);
                for (var x = 0; x < next[y].length; x++) {
                    piece[y].push({});
                    piece[y][x]['status'] = next[y][x]['status'];
                    piece[y][x]['color'] = next[y][x]['color'];
                }
                // clean(piece);
            }
            moving = true;
            nextCreated = false;

        }
        moveDown(piece);
        if (moving == true) {
            clean(piece);
            effect1(map);
            preBlock(piece);
            preSight();
            effect1(piece);

            // effect2(next);
            if (nextCreated == false) {
                index += 1;
                next = comingPiece[index];
                updateNextPieceBoard();
                nextCreated = true;
            }
        }
        createBorderForPlayField();
        play();
    }
    if (lose == true) {
        ctx.fillStyle = '#0194AA';
        ctx.fillRect(
            0,
            spaceFromTop,
            column * us + 2 * baseX + boardWidth + rangeFromGamefieldToBoard,
            row * us - 3 * us + baseX
        )
        ctx.fillStyle = 'black';
        ctx.fillRect(
            0 + effectWidth / 2,
            spaceFromTop + effectWidth / 2,
            column * us + 2 * baseX - effectWidth + boardWidth + rangeFromGamefieldToBoard,
            row * us - 3 * us + baseX - effectWidth
        )
        ctx.fillStyle = '#5FD0FC';
        ctx.font = 'Bold 60px Arial';
        ctx.fillText('GAME OVER !', baseX + 2 * us, 8 * us);
        ctx.fillStyle = "White";
        ctx.font = 'Italic 40px Times New Roman';
        ctx.fillText('Your score: ' + String(score), baseX + 2 * us, 11 * us);
        ctx.fillText('Total lines: ' + String(lines), baseX + 2 * us, 14 * us);
        ctx.fillText('Total TETRIS: ' + String(tetris), baseX + 2 * us, 17 * us);
    }
}

function play() {
    setTimeout(() => { gameRun() }, speed);
}

function speedUp() {
    speed -= 50;
}

function speedDown() {
    speed += 50;
}

play();